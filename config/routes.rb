Rails.application.routes.draw do
  resources :attachments
  get 'auth/:provider/callback', to: 'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'
  get 'sessions/create'
  get 'sessions/destroy'

  resources :sessions, only: [:create, :destroy]
  resource :home, only: [:show]
  resources :statuses
  resources :watches
  resources :votes
  
  resources :issues do
     member do
         get "set_status", to: "issues#set_status", :as => :set_status

     
     end
  end
  
  resources :comments do
    member do
    get "destroy", to: "comments#destroy", :as => :destroy
    get "pin_to_top", to: "comments#pin_to_top", :as => :pin_to_top
    get "unpin_to_top", to: "comments#unpin_to_top", :as => :unpin_to_top
  
    end
  end



  root to: "home#show"
  
  scope '/currentuser' do
    get '/' => 'users#currentuser'
  end
  
  scope '/api' do
    scope '/v1' do
      scope '/issues' do
        get '/' => 'issues_api#index'
        post '/' => 'issues_api#new'
        scope '/:id' do
          scope '/vote' do
            delete '/' => 'issues_api#deleteVote'
          end
          scope '/watch' do
            delete '/' => 'issues_api#deleteWatch'
          end
          get '/' => 'issues_api#show'
          put '/' => 'issues_api#edit'
          delete '/' => 'issues_api#delete'
          
          scope '/attachments' do
            get '/' => 'issues_api#get_attachments'
            post '/' => 'issues_api#add_attachment'
          end
          scope '/set_status' do
            post '/' => 'issues_api#set_status'
          end
        end
      end
      scope '/comments' do
        post '/' => 'comments_api#create'
        scope '/:id' do
          get '/' => 'comments_api#show'
          delete '/' => 'comments_api#destroy'
          scope '/pin_to_top' do
            put '/' => 'comments_api#pin_to_top'
          end
          scope '/unpin_to_top' do
            put '/' => 'comments_api#unpin_to_top'
          end
        end
      end
      scope '/users' do
        get '/' => 'users_api#get_all_users'
        scope '/currentuser' do
          get '/' => 'users_api#currentApiKeyUser'
        end
        scope '/:id' do
          get '/' => 'users_api#show'
        end
      end
      scope '/votes' do
        post '/' => 'votes_api#create'
        scope '/:id' do
          delete '/' => 'votes_api#destroy'
        end
      end
      scope '/watches' do
        post '/' => 'watches_api#create'
        scope '/:id' do
          delete '/' => 'watches_api#destroy'
        end
      end
    end
   end

end


