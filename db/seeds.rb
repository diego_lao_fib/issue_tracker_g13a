# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Priority.create([id: 1, name: "trivial", asset: "issues/priorities/trivial.svg"])
Priority.create([id: 2, name: "minor", asset: "issues/priorities/minor.svg"])
Priority.create([id: 3, name: "major", asset: "issues/priorities/major.svg"])
Priority.create([id: 4, name: "critical", asset: "issues/priorities/critical.svg"])
Priority.create([id: 5, name: "blocker", asset: "issues/priorities/blocker.svg"])

Kind.create([id: 1, name: "bug", asset: "issues/kinds/bug.svg"])
Kind.create([id: 2, name: "enhancement", asset: "issues/kinds/enhancement.svg"])
Kind.create([id: 3, name: "proposal", asset: "issues/kinds/proposal.svg"])
Kind.create([id: 4, name: "task", asset: "issues/kinds/task.svg"])

Status.create([id: 1, name: "new", style: "aui-lozenge aui-lozenge-subtle aui-lozenge-complete"])
Status.create([id: 2, name: "open", style: "aui-lozenge aui-lozenge-subtle aui-lozenge-default"])
Status.create([id: 3, name: "on hold", style: "aui-lozenge aui-lozenge-subtle aui-lozenge-current"])
Status.create([id: 4, name: "resolved", style: "aui-lozenge aui-lozenge-subtle aui-lozenge-success"])
Status.create([id: 5, name: "duplicate", style: "aui-lozenge aui-lozenge-subtle aui-lozenge-moved"])
Status.create([id: 6, name: "invalid", style: "aui-lozenge aui-lozenge-subtle aui-lozenge-error"])
Status.create([id: 7, name: "wontfix", style: "aui-lozenge aui-lozenge-subtle aui-lozenge-error"])
Status.create([id: 8, name: "closed", style: "aui-lozenge aui-lozenge-subtle aui-lozenge-success"])

# User.create([id: 1, name: "Diego"])
# User.create([id: 2, name: "Julen"])
# User.create([id: 3, name: "Laura"])
# User.create([id: 4, name: "Chema"])
# User.create([id: 5, name: "Marc"])
# User.create([id: 6, name: "Iñaki"])

# Issue.create([id: 1, title: "Issue1", assignee_id: 1, status_id: 1, type_id: 1, priority_id: 1, creator_id: 2])
# Issue.create([id: 2, title: "Issue2", assignee_id: 1, status_id: 2, type_id: 2, priority_id: 3, creator_id: 2])
# Issue.create([id: 3, title: "Issue3", assignee_id: 2, status_id: 3, type_id: 2, priority_id: 2, creator_id: 2])
# Issue.create([id: 4, title: "Issue4", assignee_id: 2, status_id: 3, type_id: 3, priority_id: 1, creator_id: 1])
# Issue.create([id: 5, title: "Issue5", assignee_id: 2, status_id: 1, type_id: 4, priority_id: 3, creator_id: 1])
# Issue.create([id: 6, title: "Issue6", status_id: 1, type_id: 4, priority_id: 3, creator_id: 1])
# Issue.create([id: 7, title: "Issue7", assignee_id: 2, status_id: 3, type_id: 1, priority_id: 5, creator_id: 5])
# Issue.create([id: 8, title: "Issue8", assignee_id: 3, status_id: 4, type_id: 2, priority_id: 5, creator_id: 5])
# Issue.create([id: 9, title: "Issue9", assignee_id: 5, status_id: 5, type_id: 3, priority_id: 4, creator_id: 3])
# Issue.create([id: 10, title: "Issue10", status_id: 8, type_id: 4, priority_id: 2, creator_id: 2])
# Issue.create([id: 11, title: "Issue11",assignee_id: 1, status_id: 1, type_id: 1, priority_id: 1, creator_id: 1])

# Vote.create([user_id: 1, issue_id: 1])
# Vote.create([user_id: 1, issue_id: 2])
# Vote.create([user_id: 1, issue_id: 3])
# Vote.create([user_id: 2, issue_id: 1])
# Vote.create([user_id: 2, issue_id: 2])

# Watch.create([user_id: 4, issue_id: 10])
# Watch.create([user_id: 5, issue_id: 9])
# Watch.create([user_id: 6, issue_id: 9])
# Watch.create([user_id: 5, issue_id: 8])
# Watch.create([user_id: 1, issue_id: 7])

# Comment.create([issue_id: 1,author_id: 4, text:"issue comment1",pin_to_top: 0])
# Comment.create([issue_id: 1,author_id: 5, text:"issue comment2",pin_to_top: 0])
# Comment.create([issue_id: 1,author_id: 2, text:"issue comment3",pin_to_top: 0])
# Comment.create([issue_id: 3,author_id: 3, text:"issue comment1",pin_to_top: 0])
# Comment.create([issue_id: 2,author_id: 2, text:"issue comment1",pin_to_top: 0])
# Comment.create([issue_id: 10,author_id: 1, text:"issue comment1",pin_to_top: 0])
# Comment.create([issue_id: 1,author_id: 1, text:"issue comment4",pin_to_top: 0])