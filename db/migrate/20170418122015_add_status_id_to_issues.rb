class AddStatusIdToIssues < ActiveRecord::Migration[5.0]
  def change
    add_column :issues, :status_id, :integer
  end
end
