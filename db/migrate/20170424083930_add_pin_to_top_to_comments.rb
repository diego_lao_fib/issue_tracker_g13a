class AddPinToTopToComments < ActiveRecord::Migration[5.0]
  def change
    add_column :comments, :pin_to_top, :integer
  end
end
