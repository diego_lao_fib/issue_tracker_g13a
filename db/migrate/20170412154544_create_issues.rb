class CreateIssues < ActiveRecord::Migration[5.0]
  def change
    create_table :issues do |t|
      t.string   "title"
      t.integer  "assignee_id"
      t.integer  "type_id"
      t.integer  "priority_id"
      t.text     "description"
      t.integer  "creator_id"
      
      t.timestamps
    end
  end
end
