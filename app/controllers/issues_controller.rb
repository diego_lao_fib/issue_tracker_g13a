class IssuesController < ApplicationController
  before_action :set_issue, only: [:show, :edit, :update, :destroy]
  before_action :require_login

  # GET /issues
  # GET /issues.json
  def index

    # The possible filters
    filter = params[:filter]
    sort = params[:sort]
    responsible = params[:responsible]
    kind = params[:kind]
    priority = params[:priority]
    status = params[:status]

    @issues = Issue.all

    if not status.nil? and filter.nil?
      statusArray = status.split(",")
      statusArray.each do |element|

        @issues = @issues.where(:status_id => element)

      end
    end

    if not priority.nil?
      @issues = @issues.where(:priority_id => priority)
    end

    if not kind.nil?
      @issues = @issues.where(:type_id => kind)
    end

    if not responsible.nil?
      @issues = @issues.where(:assignee_id => responsible)
    else
      case filter
        when "1"  # All
          # @issues = Issue.all
        when "2"
          @issues = @issues.where("status_id = ? OR status_id = ?", 1, 2)
        when "3"
          @issues = @issues.where(:assignee_id => session[:user_id])
        when "4"
          @issues = @issues.joins('INNER JOIN watches ON issues.id = watches.issue_id').where("watches.user_id" => session[:user_id])
        else# Open -> new&&open
          nilValues = params[:filter] == nil && params[:sort] == nil && params[:responsible] == nil && params[:kind] == nil && params[:priority] == nil && params[:status] == nil
          @issues = @issues.where("status_id = ? OR status_id = ?", 1, 2) if nilValues
      end
    end

    case sort
      when "id"
        @issues = @issues.order(id: :asc)
      when "-id"
        @issues = @issues.order(id: :desc)
      when "kind"
        @issues = @issues.order(type_id: :asc)
      when "-kind"
        @issues = @issues.order(type_id: :desc)
      when "priority"
        @issues = @issues.order(priority_id: :asc)
      when "-priority"
        @issues = @issues.order(priority_id: :desc)
      when "status"
        @issues = @issues.order(status_id: :asc)
      when "-status"
        @issues = @issues.order(status_id: :desc)
      when "votes"
        @issues = @issues.select(:id, :title, :assignee_id, :type_id, :priority_id, :description, :creator_id, :created_at, :updated_at, :status_id, "COUNT(votes.id) as issues_count").joins("LEFT OUTER JOIN votes ON issues.id = votes.issue_id").group("issues.id").order("issues_count ASC")
      when "-votes"
        @issues = @issues.select(:id, :title, :assignee_id, :type_id, :priority_id, :description, :creator_id, :created_at, :updated_at, :status_id, "COUNT(votes.id) as issues_count").joins("LEFT OUTER JOIN votes ON issues.id = votes.issue_id").group("issues.id").order("issues_count DESC")
      when "responsible"
        @issues = @issues.joins("LEFT OUTER JOIN users ON users.id = issues.assignee_id").order("case when users.name is null then 1 else 0 end, users.name ASC")
      when "-responsible"
        @issues = @issues.joins("LEFT OUTER JOIN users ON users.id = issues.assignee_id").order("case when users.name is null then 0 else 1 end, users.name DESC")
      when "created_on"
        @issues = @issues.order(created_at: :asc)
      when "-created_on"
        @issues = @issues.order(created_at: :desc)
      when "updated_on"
        @issues = @issues.order(updated_at: :asc)
      when "-updated_on"
        @issues = @issues.order(updated_at: :desc)
      else
        @issues = @issues.order(updated_at: :desc)
    end

    # Is better give the view variables with a value (not nil)
    if @issues == nil
      @issues = Array.new
    end
    
  end

  # GET /issues/1
  # GET /issues/1.json
  def show
    @comments = Comment.where("issue_id = ?", @issue.id)
    @pinToTopComments = Comment.where("pin_to_top = ? AND issue_id = ?", 1,@issue.id)
  end

  # GET /issues/new
  def new
    @issue = Issue.new
  end

  # GET /issues/1/edit
  def edit
  end

  # POST /issues
  # POST /issues.json
  def create
    logger.debug "#{params[:issue_id].to_s}"
    logger.debug "#{params[:issue_id].to_s == "-1"}"
    if(params[:issue_id].to_s == "-1")
      logger.debug "Creating issue"
      @issue = Issue.new(issue_params)
      @issue.creator_id = session[:user_id]
      @issue.status_id = 1
    
      respond_to do |format|
        if @issue.save
          if params[:files]
            params[:files].each { |file|
              @issue.attachments.create(file: file)
            }
          end
            format.html { redirect_to @issue, notice: 'Issue was successfully created.' }
            format.json { render :show, status: :created, location: @issue }
        else
            format.html { render :new }
            format.json { render json: @issue.errors, status: :unprocessable_entity }
        end
      end
    else
      logger.debug "Editing issue"
      @issue = Issue.find(params[:issue_id])
      respond_to do |format|
      if @issue.update(issue_params)
        if params[:files]
            params[:files].each { |file|
              @issue.attachments.create(file: file)
            }
        end
        format.html { redirect_to @issue, notice: 'Issue was successfully updated.' }
        format.json { render :show, status: :ok, location: @issue }
      else
        format.html { render :edit }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
    end
  
  end

  # PATCH/PUT /issues/1
  # PATCH/PUT /issues/1.json
  def update
    respond_to do |format|
      if @issue.update(issue_params)
        format.html { redirect_to @issue, notice: 'Issue was successfully updated.' }
        format.json { render :show, status: :ok, location: @issue }
      else
        format.html { render :edit }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /issues/1
  # DELETE /issues/1.json
  def destroy
    @issue.destroy
    
    respond_to do |format|
      format.html { redirect_to issues_url, notice: 'Issue was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def set_status
    if params[:status_id]=="1"
    @comment = Comment.new(:author_id => session[:user_id], :issue_id => params[:id], :text => "changed status to new", pin_to_top:2)
    elsif params[:status_id]=="2"
    @comment = Comment.new(:author_id => session[:user_id], :issue_id => params[:id], :text => "changed status to open", pin_to_top:2)
    elsif params[:status_id]=="3"
    @comment = Comment.new(:author_id => session[:user_id], :issue_id => params[:id], :text => "changed status to on hold", pin_to_top:2)
    elsif params[:status_id]=="4"
    @comment = Comment.new(:author_id => session[:user_id], :issue_id => params[:id], :text => "changed status to resolved", pin_to_top:2)
    elsif params[:status_id]=="5"
    @comment = Comment.new(:author_id => session[:user_id], :issue_id => params[:id], :text => "changed status to duplicate", pin_to_top:2)
    elsif params[:status_id]=="6"
    @comment = Comment.new(:author_id => session[:user_id], :issue_id =>params[:id], :text => "changed status to invalid", pin_to_top:2)
    elsif params[:status_id]=="7"
    @comment = Comment.new(:author_id => session[:user_id], :issue_id => params[:id], :text => "changed status to wontfix", pin_to_top:2)
    elsif params[:status_id]=="8"
    @comment = Comment.new(:author_id => session[:user_id], :issue_id => params[:id], :text => "changed status to closed", pin_to_top:2)
    end
    @comment.save
  
    @issue = Issue.find(params[:id])
    @issue.update(:status_id => params[:status_id])
    redirect_to @issue, notice: 'Status issue is changed.'
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_issue
      @issue = Issue.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def issue_params
      params.require(:issue).permit(:title, :assignee_id, :type_id, :priority_id, :status_id, :description, :creator_id, :created_at, :updated_at, :attachment)
    end

    def require_login
      if session[:user_id] == nil or !User.exists?(id: session[:user_id])
        redirect_to "/"
      end
    end
end
