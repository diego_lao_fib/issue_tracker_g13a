class SessionsController < ApplicationController
  def create
    user = User.from_omniauth(env["omniauth.auth"])
    # raise env["omniauth.auth"].to_yaml
    session[:user_id] = user.id
    session[:oauth_token] = user.oauth_token
    redirect_to "/issues"
  end

  def destroy
    session[:user_id] = nil
    session[:oauth_token] = nil
    redirect_to root_path
  end
end