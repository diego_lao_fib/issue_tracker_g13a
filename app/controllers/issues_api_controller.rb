class IssuesApiController < ApplicationController
  skip_before_filter :verify_authenticity_token  
  protect_from_forgery with: :null_session
  before_action :check_api_key
    
    
  def index
    key = request.env["HTTP_API_KEY"]
        # The possible filters
    filter = params[:filter]
    sort = params[:sort]
    responsible = params[:responsible]
    kind = params[:kind]
    priority = params[:priority]
    status = params[:status]

    @issues = Issue.all

    if not status.nil? and filter.nil?
      statusArray = status.split(",")
      statusArray.each do |element|

        @issues = @issues.where(:status_id => element)

      end
    end

    if not priority.nil?
      @issues = @issues.where(:priority_id => priority)
    end

    if not kind.nil?
      @issues = @issues.where(:type_id => kind)
    end

    if not responsible.nil?
      @issues = @issues.where(:assignee_id => responsible)
    else
      case filter
        when "1"  # All
          # @issues = Issue.all
        when "2"
          @issues = @issues.where("status_id = ? OR status_id = ?", 1, 2)
        when "3"
          user = User.find_by(api_key: key)
          @issues = @issues.where(:assignee_id => user.id)
        when "4"
          user = User.find_by(api_key: key)
          @issues = @issues.joins('INNER JOIN watches ON issues.id = watches.issue_id').where("watches.user_id" => user.id)
        else# Open -> new&&open
          nilValues = params[:filter] == nil && params[:sort] == nil && params[:responsible] == nil && params[:kind] == nil && params[:priority] == nil && params[:status] == nil
          @issues = @issues.where("status_id = ? OR status_id = ?", 1, 2) if nilValues
      end
    end

    case sort
      when "id"
        @issues = @issues.order(id: :asc)
      when "-id"
        @issues = @issues.order(id: :desc)
      when "kind"
        @issues = @issues.order(type_id: :asc)
      when "-kind"
        @issues = @issues.order(type_id: :desc)
      when "priority"
        @issues = @issues.order(priority_id: :asc)
      when "-priority"
        @issues = @issues.order(priority_id: :desc)
      when "status"
        @issues = @issues.order(status_id: :asc)
      when "-status"
        @issues = @issues.order(status_id: :desc)
      when "votes"
        @issues = @issues.select(:id, :title, :assignee_id, :type_id, :priority_id, :description, :creator_id, :created_at, :updated_at, :status_id, "COUNT(votes.id) as issues_count").joins("LEFT OUTER JOIN votes ON issues.id = votes.issue_id").group("issues.id").order("issues_count ASC")
      when "-votes"
        @issues = @issues.select(:id, :title, :assignee_id, :type_id, :priority_id, :description, :creator_id, :created_at, :updated_at, :status_id, "COUNT(votes.id) as issues_count").joins("LEFT OUTER JOIN votes ON issues.id = votes.issue_id").group("issues.id").order("issues_count DESC")
      when "responsible"
        @issues = @issues.joins("LEFT OUTER JOIN users ON users.id = issues.assignee_id").order("case when users.name is null then 1 else 0 end, users.name ASC")
      when "-responsible"
        @issues = @issues.joins("LEFT OUTER JOIN users ON users.id = issues.assignee_id").order("case when users.name is null then 0 else 1 end, users.name DESC")
      when "created_on"
        @issues = @issues.order(created_at: :asc)
      when "-created_on"
        @issues = @issues.order(created_at: :desc)
      when "updated_on"
        @issues = @issues.order(updated_at: :asc)
      when "-updated_on"
        @issues = @issues.order(updated_at: :desc)
      else
        @issues = @issues.order(updated_at: :desc)
    end

    # Is better give the view variables with a value (not nil)
    if @issues == nil
      @issues = Array.new
    end
    
    render :json => OpenStruct.new(issues: @issues).extend(IssueCollectionRepresenter)
    
  end
  
  # GET /issues/1
  # GET /issues/1.json
  def show
    
    if !Issue.exists?(id: params[:id])
      render json: {message: "issue_id no vàlid"}, status: 400
      return
    end
    
    @issue = Issue.find(params[:id])

    render :json => @issue.extend(IssueRepresenter)
    
  end
  
  # POST /issues
  # POST /issues.json
  def new
    @params =  JSON.parse(request.raw_post)
    @issue = Issue.new(@params)
    @issue.status_id = 1
    key = request.env["HTTP_API_KEY"]
    @creator = User.find_by(api_key: key)
    @issue.creator = @creator
  
    if @issue.save
      if params[:files]
        params[:files].each { |file|
          @issue.attachments.create(file: file)
        }
      end
      
      render json: {message: "Issue afegida correctament", id: @issue.id}, status: 200
    else
      render json: {message: "Issue no vàlida"}, status: 400
    end
  end

  # PATCH/PUT /issues/1
  # PATCH/PUT /issues/1.json
  def edit
    if !Issue.exists?(id: params[:id])
      render json: {message: "issue_id no vàlid"}, status: 400
      return
    end
    
    @params =  JSON.parse(request.raw_post)
    
    @issue = Issue.find(params[:id])
    if @issue.update(@params)
      if params[:files]
          params[:files].each { |file|
            @issue.attachments.create(file: file)
          }
      end
      render json: {message: "Issue editada correctament"}, status: 200
    else
      render json: {message: "Issue no vàlida"}, status: 400
    end
  end

  # DELETE /issues/1
  # DELETE /issues/1.json
  def delete
    if !Issue.exists?(id: params[:id])
      render json: {message: "issue_id no vàlid"}, status: 400
      return
    end
    
    @issue = Issue.find(params[:id])
    
    @issue.destroy
    
    render json: {message: "Issue borrada correctament"}, status: 200
  end
  
  def get_attachments
    if !Issue.exists?(id: params[:id])
      render json: {message: "issue_id no vàlid"}, status: 400
      return
    end
    
    @attachments = Attachment.where(issue_id: params[:id])
    
    render :json => @attachments
  end
  
  def add_attachment
    logger.debug params[:file].inspect
    @issue = Issue.find(params[:id])
    @issue.attachments.create(file: params[:file])
    
    render json: {message: "Arxiu adjuntat correctament"}, status: 200
  end
  
  #POST /issues/1/set_status
  #POST /issues/1/set_status.json
  def set_status
      
    if !Issue.exists?(id: params[:id])
      render json: {message: "issue_id no vàlid"}, status: 400
      return
    end
      
    if !Status.exists?(id: params[:status_id])
      render json: {message: "status_id no vàlid"}, status: 400
      return
    end
    
    key = request.env["HTTP_API_KEY"]
    @user = User.find_by(api_key: key)
      
    if params[:status_id]=="1"
      @comment = Comment.new(:author_id => @user.id, :issue_id => params[:id], :text => "changed status to new", pin_to_top:2)
    elsif params[:status_id]=="2"
      @comment = Comment.new(:author_id => @user.id, :issue_id => params[:id], :text => "changed status to open", pin_to_top:2)
    elsif params[:status_id]=="3"
      @comment = Comment.new(:author_id => @user.id, :issue_id => params[:id], :text => "changed status to on hold", pin_to_top:2)
    elsif params[:status_id]=="4"
      @comment = Comment.new(:author_id => @user.id, :issue_id => params[:id], :text => "changed status to resolved", pin_to_top:2)
    elsif params[:status_id]=="5"
      @comment = Comment.new(:author_id => @user.id, :issue_id => params[:id], :text => "changed status to duplicate", pin_to_top:2)
    elsif params[:status_id]=="6"
      @comment = Comment.new(:author_id => @user.id, :issue_id =>params[:id], :text => "changed status to invalid", pin_to_top:2)
    elsif params[:status_id]=="7"
      @comment = Comment.new(:author_id => @user.id, :issue_id => params[:id], :text => "changed status to wontfix", pin_to_top:2)
    elsif params[:status_id]=="8"
      @comment = Comment.new(:author_id => @user.id, :issue_id => params[:id], :text => "changed status to closed", pin_to_top:2)
    end
    @comment.save
    
    @issue = Issue.find(params[:id])
      
    if @issue.update(:status_id => params[:status_id])
      render json: {message: "Status canviat correctament", userid: @user.id}, status: 200
    else
      render json: {message: "Status NO canviat correctament"}, status: 400
    end
    
  end
  
  def deleteVote
    key = request.env["HTTP_API_KEY"]
    user = User.find_by(api_key: key)
    issue = Issue.find_by(id: params[:id])
    
    puts user
    puts issue
    if !issue.nil? and !issue.nil?
      vote = Vote.find_by(user_id: user.id, issue_id: issue.id)
      vote.destroy
      render json: {message: "Vote eliminat correctament"}, status: 200
    else
      render json: {message: "Vote fet prèviament no trobat"}, status: 404
    end
  end
  
  def deleteWatch
    key = request.env["HTTP_API_KEY"]
    user = User.find_by(api_key: key)
    issue = Issue.find_by(id: params[:id])
    
    puts user
    puts issue
    if !issue.nil? and !issue.nil?
      watch = Watch.find_by(user_id: user.id, issue_id: issue.id)
      watch.destroy
      render json: {message: "Watch eliminat correctament"}, status: 200
    else
      render json: {message: "Watch fet prèviament no trobat"}, status: 404
    end
    
    
  end
    
  
  private
    def read_file_data(file)
      xml_contents = ""
      if file.respond_to?(:read)
        xml_contents = file.read
      elsif file.respond_to?(:path)
        xml_contents = File.read(file.path)
      else
        logger.error "Bad file_data: #{file.class.name}: #{file.inspect}"
      end
      xml_contents
    end
    
    def check_api_key
      # Rails parse custom headers into request.env. It makes "HTTP_" + customheader.uppercase
      key = request.env["HTTP_API_KEY"]
      puts key
      if key.nil? or User.find_by(api_key: key).nil?
        render json: {message: "L’usuari no te els privilegis necessaris per realitzar l’operació"}, status: 401
      end
    end
    
end