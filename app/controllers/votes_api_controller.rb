class VotesApiController < ApplicationController
  skip_before_filter :verify_authenticity_token  
  protect_from_forgery with: :null_session
  
  before_action :check_api_key
  
  def create
    key = request.env["HTTP_API_KEY"]
    @user = User.find_by(api_key: key)
    
    if !Issue.exists?(id: params[:issue_id])
      render json: {message: "issue_id no vàlid"}, status: 400
      return
    end

    if Vote.exists?(user_id: @user.id, issue_id: params[:issue_id])
      render json: {message: "Aquest vot ja existeix"}, status: 400
      return
    end

    @vote = Vote.new(:user_id => @user.id, :issue_id => params[:issue_id])
    
    if @vote.save
      render json: {message: "Vot afegit correctament", id: @vote.id}, status: 200
      return
    else
      render json: {message: "El vot no s'ha guardat correctament"}, status: 400
      return
      
    end

  end
  
  def destroy
    key = request.env["HTTP_API_KEY"]
    if !Vote.exists?(id: params[:id])
      render json: {message: "ID no vàlid"}, status: 400
      return
    end
    
    @vote = Vote.find(params[:id])
    
    if @vote.user_id != User.find_by(api_key: key).id
      render json: {message: "Aquest vote no és teu"}, status: 401
      return
    end

    @vote.destroy
    
    render json: {message: "Vot eliminat correctament"}, status: 200
  end
  
  private
    def check_api_key
      # Rails parse custom headers into request.env. It makes "HTTP_" + customheader.uppercase
      key = request.env["HTTP_API_KEY"]
      puts key
      if key.nil? or User.find_by(api_key: key).nil?
        render json: {message: "L’usuari no te els privilegis necessaris per realitzar l’operació"}, status: 401
      end
    end
end
