class CommentsApiController < ApplicationController
  skip_before_filter :verify_authenticity_token  
  protect_from_forgery with: :null_session
  
  before_action :check_api_key
  
  # GET /comments/1
  # GET /comments/1.json
  def show
    if !Comment.exists?(id: params[:id])
      render json: {message: "issue_id no vàlid"}, status: 400
      return
    end
    
    @comment = Comment.find(params[:id])
     
    
     render :json => @comment.extend(CommentRepresenter)

  end

  # POST /comments
  # POST /comments.json
  def create

    if !Issue.exists?(id: params[:issue_id])
      render json: {message: "issue_id no vàlid"}, status: 400
      return
    end
    
    key = request.env["HTTP_API_KEY"]
    @author = User.find_by(api_key: key)
   
    @comment = Comment.new(:author_id => @author.id, :issue_id => params[:issue_id], :text => params[:text], pin_to_top:0)

    if @comment.save
        render json: {message: "Comentari creat correctament", comentari_id: @comment.id}, status: 200
    else
        render json: {message: "El comentari NO s'ha creat correctament"}, status: 400
    end
   
  end


  # PUT /comments/1/pin_to_top
  # PUT /comments/1/pin_to_top.json
  def pin_to_top
    
    if !Comment.exists?(id: params[:id], pin_to_top:0)
      render json: {message: "Commentid no vàlid"}, status: 400
      return
    end
    
    if !Issue.exists?(id: params[:issue_id])
      render json: {message: "issue_id no vàlid"}, status: 400
      return
    end
    
    @hihapin = Comment.where("pin_to_top = ? AND issue_id = ?", 1, params[:issue_id]).first
    if @hihapin != nil
      Comment.update(@hihapin.id, :pin_to_top => 0)
    end
    
    
    if Comment.update(params[:id], :pin_to_top => 1, :issue_id => params[:issue_id])
      render json: {message: "Comentari ressaltat correctament"}, status: 200
    else
       render json: {message: "El comentari NO s'ha ressaltat correctament"}, status: 400
    end
    
  end  
  
  # PUT /comments/1/unpin_to_top
  # PUT /comments/1/unpin_to_top.json
  def unpin_to_top
    
    if !Comment.exists?(id: params[:id], pin_to_top:1) 
      render json: {message: "Commentid no vàlid"}, status: 400
      return
    end
      
    if Comment.update(params[:id], :pin_to_top => 0)
        render json: {message: "Comentari des ressaltat correctament"}, status: 200
    else
        render json: {message: "El comentari NO s'ha des ressaltat correctament"}, status: 400
    end
  
  end 
  
  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    
    if !Comment.exists?(id: params[:id])
      render json: {message: "Commentid no vàlid"}, status: 400
      return
    end
    
    key = request.env["HTTP_API_KEY"]
    @user = User.find_by(api_key: key)
    
    @comment = Comment.find(params[:id])
    
    if @comment.author_id != @user.id
      render json: {message: "Aquest comentari no és teu"}, status: 401
      return
    end
   
    @comment.destroy
    render json: {message: "Comentari eliminat correctament"}, status: 200   
   
  end
  
  def check_api_key
      # Rails parse custom headers into request.env. It makes "HTTP_" + customheader.uppercase
      key = request.env["HTTP_API_KEY"]
      puts key
      if key.nil? or User.find_by(api_key: key).nil?
        render json: {message: "L’usuari no te els privilegis necessaris per realitzar l’operació"}, status: 401
      end
  end

  
end
