class UsersApiController < ApplicationController
  before_action :check_api_key
  
  def get_all_users
    
    @users = User.select('id, name, image')
    
    render :json => @users
    
  end
  
  def show
    
    @user = User.find_by(id: params[:id])
    
    if @user.nil?
      render json: {message: "user_id no vàlid"}, status: 400
      return
    end
    
    @user.api_key = nil;
    render :json => @user.to_json(:only => [:id, :name, :image, :created_at, :updated_at])
    
  end
  
  def currentApiKeyUser
    key = request.env["HTTP_API_KEY"]
    @user = User.find_by(api_key: key)
    render :json => @user.to_json(:only => [:id, :name, :image, :created_at, :updated_at])
  end
  
  private
    def check_api_key
      # Rails parse custom headers into request.env. It makes "HTTP_" + customheader.uppercase
      key = request.env["HTTP_API_KEY"]
      puts key
      if key.nil? or User.find_by(api_key: key).nil?
        render json: {message: "L’usuari no te els privilegis necessaris per realitzar l’operació"}, status: 401
      end
    end
end
