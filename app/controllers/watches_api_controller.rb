class WatchesApiController < ApplicationController
  skip_before_filter :verify_authenticity_token  
  protect_from_forgery with: :null_session
  
  before_action :check_api_key
  
  def create
    key = request.env["HTTP_API_KEY"]
    @user = User.find_by(api_key: key)
    
    if !Issue.exists?(id: params[:issue_id])
      render json: {message: "issue_id no vàlid"}, status: 400
      return
    end

    if Watch.exists?(user_id: @user.id, issue_id: params[:issue_id])
      render json: {message: "Aquest watch ja existeix"}, status: 400
      return
    end

    @watch = Watch.new(:user_id => @user.id, :issue_id => params[:issue_id])

    if @watch.save
      render json: {message: "Watch afegit correctament", id: @watch.id}, status: 200
    else
      render json: {message: "El watch no s'ha guardat correctament"}, status: 400
    end

  end
  
  def destroy
    key = request.env["HTTP_API_KEY"]
    if !Watch.exists?(id: params[:id])
      render json: {message: "ID no vàlid"}, status: 400
      return
    end
    
    @watch = Watch.find(params[:id])
    
    if @watch.user_id != User.find_by(api_key: key).id
      render json: {message: "Aquest watch no és teu"}, status: 401
      return
    end
      

    @watch.destroy
    
    render json: {message: "Watch eliminat correctament"}, status: 200
  end
  
  private
    def check_api_key
      # Rails parse custom headers into request.env. It makes "HTTP_" + customheader.uppercase
      key = request.env["HTTP_API_KEY"]
      puts key
      if key.nil? or User.find_by(api_key: key).nil?
        render json: {message: "L’usuari no te els privilegis necessaris per realitzar l’operació"}, status: 401
      end
    end
end
