class WatchesController < ApplicationController
  before_action :set_watch, only: [:show, :edit, :update, :destroy]
  before_action :require_login

  # GET /watches
  # GET /watches.json
  def index
    @watches = Watch.all
  end

  # GET /watches/1
  # GET /watches/1.json
  def show
  end

  # GET /watches/new
  def new
    @watch = Watch.new
  end

  # GET /watches/1/edit
  def edit
  end

  # POST /watches
  # POST /watches.json
  def create

    if params[:token] != User.find(params[:user_id]).oauth_token
      redirect_to :back
      return
    end

    if Watch.exists?(user_id: params[:user_id], issue_id: params[:issue_id])
      redirect_to :back
      return
    end

    @watch = Watch.new(:user_id => params[:user_id], :issue_id => params[:issue_id])

    respond_to do |format|
      if @watch.save
        format.html { redirect_to :back, notice: 'Watch was successfully created.' }
        format.json { redirect :show, status: :created, location: @watch }
      else
        format.html { render :new }
        format.json { render json: @watch.errors, status: :unprocessable_entity }
      end
    end

  end

  # PATCH/PUT /watches/1
  # PATCH/PUT /watches/1.json
  def update
    respond_to do |format|
      if @watch.update(watch_params)
        format.html { redirect_to @watch, notice: 'Watch was successfully updated.' }
        format.json { render :show, status: :ok, location: @watch }
      else
        format.html { render :edit }
        format.json { render json: @watch.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /watches/1
  # DELETE /watches/1.json
  def destroy


    if params[:token] != User.find(@watch.user_id).oauth_token
      redirect_to :back
      return
    end

    @watch.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Watch was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_watch
      @watch = Watch.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def watch_params
      # session.each do |elem|
      #   puts elem
      # end
      # params.each do |elem|
      #   puts elem
      # end
      params.fetch(:watch)
    end
    
    def require_login
      if session[:user_id] == nil or !User.exists?(id: session[:user_id])
        redirect_to "/"
      end
    end
end
