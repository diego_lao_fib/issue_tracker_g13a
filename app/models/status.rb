class Status < ApplicationRecord
    has_many :issues, :class_name => 'Issue', :foreign_key => 'status_id'
end
