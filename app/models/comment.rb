class Comment < ApplicationRecord
  belongs_to :creator, :class_name => 'User', :foreign_key => 'author_id'
  belongs_to :issue, :class_name => 'Issue', :foreign_key => 'issue_id'

end
