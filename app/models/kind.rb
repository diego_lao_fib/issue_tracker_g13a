class Kind < ApplicationRecord
  has_many :issues, :class_name => 'Issue', :foreign_key => 'type_id'
end
