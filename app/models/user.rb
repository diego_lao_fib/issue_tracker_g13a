require 'digest/md5'

class User < ActiveRecord::Base
  has_many :created_issues, :class_name => 'Issue', :foreign_key => 'creator_id'
  has_many :assigned_issues, :class_name => 'Issue', :foreign_key => 'assignee_id'
  has_and_belongs_to_many :voted_issues, :class_name => 'Issue', :join_table => 'Votes'
  has_and_belongs_to_many :watch_issues, :class_name => 'Issue', :join_table => 'Watches'
  has_many :created_comments, :class_name => 'Comment', :foreign_key => 'author_id'

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.image = auth.info.image
      user.api_key = Digest::MD5.hexdigest(user.uid + user.name)
      user.save!
    end
  end
end