class Priority < ApplicationRecord
  has_many :issues, :class_name => 'Issue', :foreign_key => 'priority_id'
end
