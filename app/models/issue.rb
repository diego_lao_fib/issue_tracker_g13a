class Issue < ApplicationRecord
  extend IssueRepresenter
  belongs_to :creator, :class_name => 'User', :foreign_key => 'creator_id'
  belongs_to :assignee, :class_name => 'User', :foreign_key => 'assignee_id'
  belongs_to :kind, :class_name => 'Kind', :foreign_key => 'type_id'
  belongs_to :priority, :class_name => 'Priority', :foreign_key => 'priority_id'
  belongs_to :status, :class_name => 'Status', :foreign_key => 'status_id'
  has_and_belongs_to_many :voters, :class_name => 'User', :join_table => 'votes'
  has_and_belongs_to_many :watchers, :class_name => 'User', :join_table => 'watches'
  has_many :attachments, :class_name => 'Attachment', :foreign_key => 'issue_id'
  has_many :comments, :class_name => 'Comment', :foreign_key => 'issue_id'
end
