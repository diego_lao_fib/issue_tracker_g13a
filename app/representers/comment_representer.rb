require 'roar/json'
require 'roar/json/hal'

module CommentRepresenter
  include Roar::JSON::HAL
  
  link :self do
    "http://issue-tracker-g13a.herokuapp.com/api/v1/comments/#{id}"
  end

  property :id
  
  property :created_at
  
  property :creator, extend: UserRepresenter, class: User

  property :text
  
  property :issue, extend: IssueRepresenter, class: Issue

  
end