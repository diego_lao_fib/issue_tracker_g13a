require 'roar/json'
require 'roar/json/hal'

module IssueCollectionRepresenter
  include Roar::JSON::HAL

  link :self do
    "http://issue-tracker-g13a.herokuapp.com/api/v1/issues"
  end
  
  collection :issues, extend: IssueRepresenter, class: Issue
  
end