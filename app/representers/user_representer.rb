require 'roar/json'
require 'roar/json/hal'

module UserRepresenter
  include Roar::JSON::HAL
  
  link :self do
    "http://issue-tracker-g13a.herokuapp.com/api/v1/users/#{id}"
  end
  
  property :id
  
  property :name

end