require 'roar/json'
require 'roar/json/hal'

module IssueRepresenter
  include Roar::JSON::HAL
  
  link :self do
    "http://issue-tracker-g13a.herokuapp.com/api/v1/issues/#{id}"
  end
  
  property :id
  
  property :title
  
  property :assignee, extend: UserRepresenter, class: User
  
  property :kind, class: Kind do
    property :id
    property :name
  end
  
  property :priority, class: Priority do
    property :id
    property :name
  end
  
  property :description
  
  property :creator, extend: UserRepresenter, class: User
  
  property :created_at
  
  property :updated_at
  
  property :status, class: Status do
    property :id
    property :name
  end
  
  collection :voters, extend: UserRepresenter, class: User
  
  collection :watchers, extend: UserRepresenter, class: User
  
  collection :attachments, class: Attachment do
    property :file_file_name
  end
  
  collection :comments, class: Comment do
    property :id
    property :author_id
    property :text
  end
  
end