json.array!(@issues) do |issue|
  json.extract! issue, :id, :title, :assignee_id, :type_id, :priority_id, :status_id, :description, :creator_id, :created_at, :updated_at
  json.url issue_url(issue, format: :json)
end
