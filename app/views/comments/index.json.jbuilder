json.array!(@comments) do |comment|
  json.extract! comment, :id, :author_id, :text, :issue_id
  json.url comment_url(comment, format: :json)
end
