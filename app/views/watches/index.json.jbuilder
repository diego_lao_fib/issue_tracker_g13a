json.array!(@watches) do |watch|
  json.extract! watch, :id
  json.url watch_url(watch, format: :json)
end
