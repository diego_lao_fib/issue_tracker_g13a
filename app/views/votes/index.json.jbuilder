json.array!(@votes) do |vote|
  json.extract! vote, :id, :user_id, :issue_id
  json.url vote_url(vote, format: :json)
end
